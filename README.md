# gfp-front-vue

## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve || npm run start
```
### Compiles and minifies for production
```
npm run build
```
### Lints and fixes files
```
npm run lint
```
## Project Configuration

- Port: 7666


## Pre-registered Users

The project includes the following pre-registered users for login:

- E-mail: admin@admin.com.br, Password: admin@123
- E-mail: member1@member.com.br, Password: member@123
- E-mail: member2@member.com.br, Password: member@123

You can use these credentials to log in to the application and test the authentication features.

## Project Details

This project was developed with Vue 3, TypeScript, SCSS, and Bootstrap 5. It utilizes Firebase Authentication and Firebase Database for authentication and database functionalities. The project follows the MVP (Model-View-Presenter) architecture pattern and incorporates the Adapter design pattern for certain implementations.

## Environment Setup

- Node version: 14.21.3
- npm version: 6.14.18

Make sure to have Node.js and npm installed on your machine before running the project.

## Firebase Integration

The project integrates Firebase Authentication for user authentication and Firebase Database for data storage and retrieval. The Firebase configuration is set up in the appropriate files, allowing seamless integration with Firebase services.

## Style and Theming

The project uses SCSS (Sass) for styling and leverages the Bootstrap 5 framework for responsive and customizable UI components. The styles are organized in separate SCSS files and imported as needed.

## Building and Deployment

To build the project for production, run `npm run build`. This will create a `dist` directory with the compiled and minified files ready for deployment. The project can then be deployed to a web server or hosting service of your choice.

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).