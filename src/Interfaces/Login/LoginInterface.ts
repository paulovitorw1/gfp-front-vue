export interface Login {
    email: string,
    password: string
}

export interface ResponseLogin {
    uid: string
    name: string
    email: string
    role: string
}