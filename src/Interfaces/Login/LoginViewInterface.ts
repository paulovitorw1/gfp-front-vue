import { Login } from "./LoginInterface"
export interface LoginViewInterface {
    doLogin(data: Login): void;
}

export interface LoginPresenterInterface {
    doLogin(data: Login): void;
}