export interface AdapterListFeedbacks {
    title: string
    description: string
    inforBottom: string
}