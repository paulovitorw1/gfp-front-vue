import { AdapterListFeedbacks } from "./AdapterListFeedbacks";
import { FeedbackFormInterface } from "./FeedbackInterface";
import {Product} from "@/Interfaces/ListProduct/ProductInterface"
export interface OverviewProductViewInterface {
    showModal(): void;
    hideModal(): void
    sendFeedback(): void
    getProduct(id: string): void
    setProduct(product: Product): void
    getFeedbacks(id: string): void;
    setFeedbacks(feedbacks: AdapterListFeedbacks[]): void
}

export interface OverviewProductPresenterInterface {
    sendFeedback(feedback: FeedbackFormInterface): void
    getProduct(id: string): void
    getFeedbacks(id: string): void;
}

export interface OverviewProductRestInterface {
    sendFeedback(feedback: FeedbackFormInterface): void
    getProduct(id: string): void
    getFeedbacks(id: string): void;
}