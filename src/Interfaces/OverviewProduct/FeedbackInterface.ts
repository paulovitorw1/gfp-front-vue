export interface FeedbackFormInterface {
    idProduct: string
    nameUser: string
    title: string
    description: string
    created_at: string
}
