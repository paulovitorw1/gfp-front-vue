export interface Product {
    id: string | undefined,
    urlImage: string,
    code: string,
    name: string,
    description: string,
    model: string,
    height: string,
    width: string,
    depth: string
}