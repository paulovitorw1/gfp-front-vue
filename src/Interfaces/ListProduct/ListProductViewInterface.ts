import { AdaptedProduct } from "./AdapterListProducts";
import { Product } from "./ProductInterface";

export interface ListProductViewInterface {
    showModal(): void;
    createProduct(): void
    showError(message: string): void;
    productCreatedSuccess(): void;
    resetForm(): void;
    setProductsView(products: AdaptedProduct[]): void
}

export interface ListProductPresenterInterface {
    createProduct(product: Product): void;
    validationForm(product: Product): void;
    getProducts(): void;
}