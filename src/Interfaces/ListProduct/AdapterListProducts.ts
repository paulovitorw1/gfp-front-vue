export interface AdaptedProduct {
    id: string;
    urlImage: string;
    name: string;
    description: string;
}