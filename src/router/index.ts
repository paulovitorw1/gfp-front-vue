import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Login from '../views/Login/Login.vue'
import ListProduct from '../views/ListProduct/ListProduct.vue'
import OverviewProduct from '../views/OverviewProduct/OverviewProduct.vue'
import { LocalStorageService } from '@/services/LocalStorageService'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'ListProduct',
    component: ListProduct
  },
  {
    path: '/produto/:id',
    name: 'OverviewProduct',
    component: OverviewProduct,
    props: true,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  const userRole = checkRoles()
    if(userRole != null){
      to.meta.role = userRole.role;
    }
    // Prossiga para a próxima rota
    next();
});

function checkRoles() {
  return LocalStorageService.getUser()
}

export default router
