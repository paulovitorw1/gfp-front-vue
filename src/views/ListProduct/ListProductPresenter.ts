import { ListProductPresenterInterface, ListProductViewInterface } from "@/Interfaces/ListProduct/ListProductViewInterface";
import { Product } from "@/Interfaces/ListProduct/ProductInterface";
import { adaptProducts } from "@/model/AdapterProducts";
import { ListProductRest } from "@/services/api/ListProductRest";

export class ListProductPresenter implements ListProductPresenterInterface {
    private view: ListProductViewInterface;
    private _service: ListProductRest

    constructor(view: ListProductViewInterface) {
        this.view = view;
        this._service = new ListProductRest();
    }

    getProducts(): void {
        this._service
        .getProducts()
        .then((response) => {
            const products = adaptProducts(response)
            this.view.setProductsView(products)
        })
        .catch((error) => {
            this.view.showError(error);
            throw error;
        });
    }

    createProduct(product: Product): void {
        if (!this.validationForm(product)) return;
        this._service
            .createProduct(product)
            .then(() => {
                this.view.productCreatedSuccess()
            })
            .catch((error) => {
                this.view.showError(error);
                throw error;
            });
    }
    validationForm(product: Product): boolean {
        const { code, name, description, model, height, width, depth } = product;

        if (!code || !name || !description || !model || !height || !width || !depth) {
            this.view.showError("Preencha todos os campos obrigatórios");
            return false;
        }
        return true;
    }
}