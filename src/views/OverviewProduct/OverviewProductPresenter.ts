import { Product } from "@/Interfaces/ListProduct/ProductInterface";
import { FeedbackFormInterface } from "@/Interfaces/OverviewProduct/FeedbackInterface";
import { OverviewProductPresenterInterface, OverviewProductViewInterface } from "@/Interfaces/OverviewProduct/OverviewProductViewInterface";
import { adaptFeedbacks } from "@/model/AdapterFeedbacks";
import { OverviewProductRest } from "@/services/api/OverviewProductRest";

export class OverviewProductPresenter implements OverviewProductPresenterInterface {
  private view: OverviewProductViewInterface;
  private _service: OverviewProductRest

  constructor(view: OverviewProductViewInterface) {
    this.view = view;
    this._service = new OverviewProductRest();
  }

  getProduct(id: string): void {
    this._service
      .getProduct(id)
      .then((data: Product) => {
        this.view.setProduct(data)
      })
      .catch((error) => {
        throw error;
      });
  }

  getFeedbacks(id: string): void {
    this._service
      .getFeedbacks(id)
      .then((data: FeedbackFormInterface[]) => {
        const adapter = adaptFeedbacks(data)
        this.view.setFeedbacks(adapter)
      })
      .catch((error) => {
        throw error;
      });
  }

  sendFeedback(feedback: FeedbackFormInterface): void {
    this._service
      .sendFeedback(feedback)
      .then(() => {
       this.view.hideModal()
      })
      .catch((error) => {
        throw error;
      });
  }
}