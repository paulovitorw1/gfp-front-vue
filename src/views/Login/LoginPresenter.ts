import { Login, ResponseLogin } from "@/Interfaces/Login/LoginInterface";
import { LoginPresenterInterface, LoginViewInterface } from "@/Interfaces/Login/LoginViewInterface";
import router from "@/router";
import { LocalStorageService } from "@/services/LocalStorageService";
import { LoginRest } from "@/services/api/Login.rest";

export class LoginPresenter implements LoginPresenterInterface {
    private view: LoginViewInterface;
    private _service: LoginRest

    constructor(view: LoginViewInterface) {
        this.view = view;
        this._service = new LoginRest();
    }

    doLogin(data: Login): void {
        this._service
            .doLogin(data)
            .then((response: ResponseLogin) => {
                LocalStorageService.setUser(response)
                router.push({ name: "ListProduct" });
            })
            .catch((error) => {
                throw error;
            });
    }
}