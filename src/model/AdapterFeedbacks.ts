import { AdapterListFeedbacks } from "@/Interfaces/OverviewProduct/AdapterListFeedbacks";
import { FeedbackFormInterface } from "@/Interfaces/OverviewProduct/FeedbackInterface";

export function adaptFeedbacks(feedbacks: FeedbackFormInterface[]): AdapterListFeedbacks[] {
    return feedbacks.map((feedback) => {
        return {
            title: feedback.title,
            description: feedback.description,
            inforBottom: `${feedback.nameUser} em ${formatedDate(feedback.created_at)}`
        };
    });
}

 function formatedDate(dataString: string): string {
    const data = new Date(dataString);
    const day = data.getDate().toString().padStart(2, '0');
    const month = (data.getMonth() + 1).toString().padStart(2, '0');
    const year = data.getFullYear().toString();
    const hours = data.getHours().toString().padStart(2, '0');
    const minutes = data.getMinutes().toString().padStart(2, '0');

    return `${day}/${month}/${year} às ${hours}:${minutes}`;
}