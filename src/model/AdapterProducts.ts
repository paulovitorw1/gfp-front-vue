import { AdaptedProduct } from "@/Interfaces/ListProduct/AdapterListProducts";
import { Product } from "@/Interfaces/ListProduct/ProductInterface";

export function adaptProducts(products: Product[]): AdaptedProduct[] {
    return products.map((product) => {
        return {
            id: product.id as string,
            urlImage: product.urlImage,
            name: product.name,
            description: product.description
        };
    });
}
