// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDojz26FA1rEcvxbXyOw2b4yuCWKuBxfEU",
  authDomain: "gfp-core.firebaseapp.com",
  projectId: "gfp-core",
  storageBucket: "gfp-core.appspot.com",
  messagingSenderId: "906171326910",
  appId: "1:906171326910:web:b263249f2adb103778d9ef",
  measurementId: "G-TD7KJ8KQ5W"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);
const db = getFirestore(firebaseApp);
export default db;
export { auth };
