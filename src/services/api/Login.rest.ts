import { User, signInWithEmailAndPassword } from "firebase/auth";
import db, { auth } from "@/services/firebaseDb";
import { Login, ResponseLogin } from "@/Interfaces/Login/LoginInterface";
import { DocumentData, doc, getDoc } from "firebase/firestore";

export class LoginRest {
    doLogin(data: Login): Promise<ResponseLogin> {
        const { email, password } = data;
        let user: User;

        return signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                user = userCredential.user as User;
                return this.getUserData(user.uid);
            })
            .then((dataUser) => {
                const response: ResponseLogin = {
                    uid: user.uid ?? "",
                    name: dataUser?.name ?? "",
                    email: user.email ?? "",
                    role: dataUser?.role ?? ""
                };

                return response;
            })
            .catch((error) => {
                throw error;
            });
    }

    getUserData(userId: string): Promise<DocumentData | undefined> {
        const userDocRef = doc(db, "users", userId);
        return getDoc(userDocRef)
            .then((userDocSnapshot) => {
                return userDocSnapshot.data();
            });
    }
}
