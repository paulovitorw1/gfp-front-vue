import db from "@/services/firebaseDb";
import { getDocs, collection, query, where, addDoc } from "firebase/firestore";
import { Product } from "@/Interfaces/ListProduct/ProductInterface";

export class ListProductRest {
    getProducts(): Promise<Product[]> {
        const productsRef = collection(db, "products");

        return getDocs(productsRef).then((querySnapshot) => {
            const products = [] as Product[]
            querySnapshot.forEach((doc) => {
                const product = doc.data();
                const productData = {
                    id: doc.id,
                    urlImage: product.urlImage,
                    code: product.code,
                    name: product.name,
                    description: product.description,
                    model: product.model,
                    height: product.height,
                    width: product.width,
                    depth: product.depth
                } as Product
                products.push(productData);
            });
            return products;
        });
    }

    createProduct(product: Product): Promise<Product> {
        return this.checkProductExistence(product.code).then(() => {
            return this.saveProduct(product);
        });
    }

    private checkProductExistence(code: string): Promise<void> {
        const productRef = collection(db, "products");
        const productQuery = query(productRef, where("code", "==", code));

        return getDocs(productQuery).then((querySnapshot) => {
            if (!querySnapshot.empty) {
                throw new Error("O número do produto já existe");
            }
        });
    }

    private saveProduct(product: Product): Promise<Product> {
        const productRef = collection(db, "products");
        return addDoc(productRef, product).then(() => product)
    }
}