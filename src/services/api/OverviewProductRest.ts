import { FeedbackFormInterface } from "@/Interfaces/OverviewProduct/FeedbackInterface"
import { OverviewProductRestInterface } from "@/Interfaces/OverviewProduct/OverviewProductViewInterface";
import { collection, addDoc, doc, getDoc, DocumentData, getDocs, where, query } from "firebase/firestore";
import db from "@/services/firebaseDb";
import { Product } from "@/Interfaces/ListProduct/ProductInterface";

export class OverviewProductRest implements OverviewProductRestInterface {
  getFeedbacks(id: string): Promise<FeedbackFormInterface[]> {
    const feedbacksRef = collection(db, "product-feedbacks");
    const feedbacksQuery = query(feedbacksRef, where("idProduct", "==", id));
    
    return getDocs(feedbacksQuery).then((querySnapshot) => {
      const feedbacks = [] as FeedbackFormInterface[]
      querySnapshot.forEach((doc) => {
        const feedback = doc.data();
        const feedbacksData = {
          idProduct: feedback.idProduct,
          nameUser: feedback.nameUser,
          title: feedback.title,
          description: feedback.description,
          created_at: feedback.created_at,
        } as FeedbackFormInterface
        feedbacks.push(feedbacksData);
      });
      return feedbacks;
    });
  }
  getProduct(id: string): Promise<Product> {
    const productRef = doc(db, "products", id);

    return getDoc(productRef)
      .then((docSnapshot) => {
        const productData = docSnapshot.data() as DocumentData;
        const product: Product = {
          id: id,
          urlImage: productData.urlImage,
          code: productData.code,
          name: productData.name,
          description: productData.description,
          model: productData.model,
          height: productData.height,
          width: productData.width,
          depth: productData.depth
        };
        return product
      })
  }
  sendFeedback(feedback: FeedbackFormInterface): Promise<void> {
    const collectionRef = collection(db, "product-feedbacks");
    return addDoc(collectionRef, feedback).then(() => {
      // Trate o feedback aqui no presenter
    });
  }
}