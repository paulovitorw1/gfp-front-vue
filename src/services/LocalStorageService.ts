import { ResponseLogin } from "@/Interfaces/Login/LoginInterface";

export class LocalStorageService {
    private static readonly USER_KEY = "authentication";
  
    static setUser(user: ResponseLogin): void {
      localStorage.setItem(LocalStorageService.USER_KEY, JSON.stringify(user));
    }
  
    static getUser(): ResponseLogin | null {
      const userString = localStorage.getItem(LocalStorageService.USER_KEY);
      if (userString) {
        return JSON.parse(userString) as ResponseLogin;
      }
      return null;
    }
  
    static clearUser(): void {
      localStorage.removeItem(LocalStorageService.USER_KEY);
    }
  }